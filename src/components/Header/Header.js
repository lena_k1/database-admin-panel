import React, { useState} from "react";
import { Link} from "react-router-dom";
import styles from "./Header.module.scss";
import { toast } from "react-toastify";
import { Collapse, Navbar, NavbarToggler, Nav, NavItem } from "reactstrap";

const Header = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  return (
    <Navbar light expand="xl" className={styles.navbar}>
      <Link to="/">
        <h2 className={styles.title} onClick={() => setIsOpen(false)}>
          <i className="fas fa-address-card" />
          Admin Panel
        </h2>
      </Link>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar className={styles.collapse}>
        <Nav className="mr-auto" navbar className={styles.nav}>
          <NavItem className={styles.item} onClick={toggle}>
            <Link to="/">List of unemployed</Link>
          </NavItem>
          <NavItem className={styles.item} onClick={toggle}>
            <Link to="/add">Add unemployed</Link>
          </NavItem>
          <NavItem
            className={styles.item}
            onClick={() => {
              toggle();
              localStorage.setItem("isLogin", false);
              toast.success("Logged out!");
            }}
          >
            <Link to="/login">Logout</Link>
          </NavItem>
        </Nav>
      </Collapse>
    </Navbar>
  );
};

export default Header;
