import React, { useState } from "react";
import styles from "./Form.module.scss";
import { Form, FormGroup, Label, Input, Button, Tooltip } from "reactstrap";
import Select from "react-select";
import classNames from "classnames";

import { addInputs, activityOptions } from "../../config/constants";

const FormContainer = ({
  handleChange,
  handleSubmit,
  values,
  btnTitle,
  isDelete = false,
  handleDeleteButton,
  arrayOfFields,
  edit
}) => {
  const [tooltipOpen, setTooltipOpen] = useState({
    first: false,
    second: false,
  });
  const fields = arrayOfFields ? arrayOfFields : addInputs;
  return (
    <Form onSubmit={handleSubmit}>
      {fields.map(
        ({ name, type, placeholder, required, maxLength, minLength }, key) => (
          <FormGroup key={key}>
            {type !== "select" ? (
              <>
                {" "}
                <Label className={styles.label}>
                  {placeholder}{" "}
                  <span
                    style={{
                      color: "red",
                      cursor:
                        name === "identificationCode" || name === "passportID"
                          ? "pointer"
                          : "default",
                    }}
                    id={`tooltip${key}`}
                  >
                    *
                  </span>
                </Label>
                <Input
                  id={key}
                  className={classNames(styles.input, edit && name === "identificationCode" && styles.disabled)}
                  onChange={handleChange}
                  name={name}
                  type={type}
                  min={0}
                  placeholder={placeholder}
                  required={required}
                  value={values[name]}
                  disabled={edit && name === "identificationCode" ? true : false}
                  maxLength={maxLength}
                  minLength={minLength}
                  onInput={(e) => {
                    if (maxLength) {
                      e.target.value = e.target.value.replace(/[^0-9]/gi, "");
                    }
                  }}
                />
                {name === "identificationCode" || name === "passportID" ? (
                  <Tooltip
                    placement="right"
                    isOpen={
                      name === "passportID"
                        ? tooltipOpen.first
                        : tooltipOpen.second
                    }
                    target={`tooltip${key}`}
                    toggle={() => {
                      if (name === "identificationCode") {
                        setTooltipOpen((prev) => ({
                          ...prev,
                          second: !tooltipOpen.second,
                        }));
                      } else
                        setTooltipOpen((prev) => ({
                          ...prev,
                          first: !tooltipOpen.first,
                        }));
                    }}
                  >
                    {`${placeholder} must contain ${maxLength} characters`}
                  </Tooltip>
                ) : null}
              </>
            ) : (
              <>
                <Label className={styles.label}>
                  Activity{" "}
                  <span
                    style={{
                      color: "red",
                    }}
                  >
                    *
                  </span>
                </Label>
                <Select
                  defaultValue={{ label: "Worker", value: "worker" }}
                  options={activityOptions}
                  onChange={handleChange}
                  className={styles.select}
                  value={{ label: values.activity, value: values.activity }}
                />
              </>
            )}
          </FormGroup>
        )
      )}

      <Button className={styles.btn} type="submit" color="success">
        {btnTitle}
      </Button>
      {isDelete && (
        <Button
          className={classNames(styles.btn, styles.deleteBtn)}
          type="button"
          color="danger"
          onClick={handleDeleteButton}
        >
          Delete unemployed
        </Button>
      )}
    </Form>
  );
};

export default FormContainer;
