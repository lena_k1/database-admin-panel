import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";

import { columns } from "../../config/tableOptions";
import styles from "./Table.module.scss";

const DataTable = ({ data }) => {
  const { SearchBar } = Search;

  return (
    <ToolkitProvider
      keyField="1"
      data={data}
      columns={columns}
      search
    >
      {(props) => (
        <div className={styles.wrapper}>
          <SearchBar {...props.searchProps} />
          <hr />
            <BootstrapTable
              {...props.baseProps}
              noDataIndication="Table is empty"
              hover
              keyField="1"
              condensed
              striped
              bootstrap4
              classes={styles.table}
              wrapperClasses={styles.scrollTable}
            />
        </div>
      )}
    </ToolkitProvider>
  );
};

export default DataTable;
