import axios from "axios";

export default class Login {
  static login(credentials) {
    return axios.post(`https://nameless-journey-95733.herokuapp.com/admin/login`, credentials);
  }
}
