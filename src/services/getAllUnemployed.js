import api from "./api";

export default class AllUnemployed {
  static get() {
    return api.get(`/unemployed`);
  }
}
