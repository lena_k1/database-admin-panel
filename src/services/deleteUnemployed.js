import api from "./api";

export default class DeleteUnemployed {
  static delete(id) {
    return api.delete(`/delete-unemployed/${id}`);
  }
}
