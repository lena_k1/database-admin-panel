import api from "./api";

export default class UpdateUnemployed {
  static update(updatedUnemployed) {
    return api.put(`/update-unemployed`, updatedUnemployed);
  }
}
