import api from "./api";

export default class AddUnemployed {
  static add(newUnemployed) {
    return api.post(`/add-unemployed`, newUnemployed);
  }
}
