import api from "./api";

export default class GetUnemployed {
  static find(id) {
    return api.get(`/get-unemployed/${id}`);
  }
}
