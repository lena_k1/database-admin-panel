import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "bootstrap/dist/css/bootstrap.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import 'react-toastify/dist/ReactToastify.css';
import ContextProvider from "./context/context";

ReactDOM.render(<ContextProvider><App /></ContextProvider>, document.getElementById("root"));
