import React, { useContext, useEffect, useState } from "react";
import { Route, Redirect } from "react-router-dom";
import { Context } from "../context/context";
import Header from "../components/Header/Header";
import Login from "../pages/Login/Login";

export const PublicRoute = ({ component: Component, restricted, ...rest }) => {
  const { isLogin, setIsLogin } = useContext(Context);
  setIsLogin(Boolean(Number(localStorage.getItem("isLogin"))));
  console.log(isLogin);
  if (isLogin)
    return (
      <Route {...rest}>
        <Redirect to={"/"} />
        <Component {...rest} />
      </Route>
    );
  const renderComponent = (props) => <Component {...rest} />;
  return <Route {...rest} render={renderComponent} />;
};

export const PrivateRoute = ({ component: Component, restricted, ...rest }) => {
  const { isLogin, setIsLogin } = useContext(Context);
  // useEffect(() => {
  //   setIsLogin(Boolean(Number(localStorage.getItem("isLogin"))));
  //   // console.log(Boolean(Number(localStorage.getItem("isLogin"))))
  // }, [])
  setIsLogin(Boolean(Number(localStorage.getItem("isLogin"))));
  // setIsLogin(Boolean(localStorage.getItem("isLogin")));
  // useEffect(() => {
  //   if (localStorage.getItem("isLogin") === "false") setIsLogin(false);
  //   console.log(isLogin);
  // }, []);
  // if (localStorage.getItem("isLogin") === "false") setIsLogin(false)
  console.log(isLogin)
  return (
    <Route
      {...rest}
      render={(props) =>
        isLogin ? (
          <>
            <>
              <Header />
              <Component {...props} />
            </>
            {/* <Redirect to="/login" /> */}
            {/* <Route exact path="/login">
              <Login />
            </Route> */}
          </>
        ) : (
          <Route {...rest}>
            <Redirect to={"/login"} />
            <Login />
          </Route>
          // <Redirect to="/login" />
          // <>
          //   <Header />
          //   <Component {...props} />
          // </>
        )
      }
    />
  );
};
