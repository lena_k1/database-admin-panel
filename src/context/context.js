import { createContext, useState } from "react";

export const Context = createContext();

const ContextProvider = (props) => {
  const [isLogin, setIsLogin] = useState(Boolean(localStorage.getItem("isLogin")));

  return (
    <Context.Provider
      value={{
        isLogin,
        setIsLogin,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};
export default ContextProvider;
