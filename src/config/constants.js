export const API_URL = "https://nameless-journey-95733.herokuapp.com/api";

export const addInputs = [
  {
    name: "firstName",
    placeholder: "First Name",
    type: "text",
    required: true,
  },
  {
    name: "lastName",
    placeholder: "Last Name",
    type: "text",
    required: true,
  },
  {
    name: "activity",
    placeholder: "Activity",
    type: "select",
    required: true,
  },
  {
    name: "passportID",
    placeholder: "Passport ID",
    type: "text",
    required: true,
    maxLength: 9,
    minLength: 8,
  },
  {
    name: "identificationCode",
    placeholder: "Identification Code",
    type: "text",
    required: true,
    maxLength: 10,
  },
];

export const loginInputs = [
  {
    name: "email",
    placeholder: "Email",
    type: "email",
    required: true,
  },
  {
    name: "password",
    placeholder: "Password",
    type: "password",
    required: true,
  }
];

export const activityOptions = [
  { value: "IT", label: "IT" },
  { value: "Cooking", label: "Cooking" },
  { value: "Education", label: "Education" },
  { value: "Cosmetology", label: "Cosmetology" },
  { value: "Business", label: "Business" },
  { value: "Selling", label: "Selling" },
  { value: "Delivery", label: "Delivery" },
  { value: "HealthCare", label: "Health Care" },
  { label: "Worker", value: "worker" },
];
