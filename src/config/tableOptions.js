import { Button } from "reactstrap";
import { Link } from "react-router-dom";

const EditButton = (cell, row, rowIndex) => {
  return (
    <Link to={`/edit/${row.id}`}>
      <Button color="success">
        <i className="fas fa-user-edit" />
      </Button>
    </Link>
  );
};
const commonStylesForColumn = {
  textTransform: "uppercase",
  color: "#348f50",
};

const styleForRows = {
  overflow: "hidden",
  textOverflow: "ellipsis",
  padding: "10px 10px",
};
export const columns = [
  {
    dataField: "id",
    text: "ID",
    headerStyle: () => {
      return { ...commonStylesForColumn, width: "100px" };
    },
    sort: true,
    sortFunc: (a, b, order) => {
      if (order === "asc") {
        return Number(b - a);
      }
      return Number(a - b);
    },
    style: { ...styleForRows, fontWeight: 800 },
  },
  {
    dataField: "firstName",
    text: "First Name",
    headerStyle: () => {
      return { ...commonStylesForColumn, width: "140px" };
    },
    sort: true,
    style: { ...styleForRows },
  },
  {
    dataField: "lastName",
    text: "Last Name",
    headerStyle: () => {
      return { ...commonStylesForColumn, width: "140px" };
    },
    sort: true,
    style: { ...styleForRows },
  },
  {
    dataField: "passportID",
    text: "Passport ID",
    headerStyle: () => {
      return { ...commonStylesForColumn, width: "150px" };
    },
    style: { ...styleForRows },
  },
  {
    dataField: "activity",
    text: "Activity",
    headerStyle: () => {
      return { ...commonStylesForColumn, width: "100px" };
    },
    style: { ...styleForRows },
  },
  {
    dataField: "identificationCode",
    text: "Identification Code",
    headerStyle: () => {
      return { ...commonStylesForColumn, width: "180px" };
    },
    style: { ...styleForRows },
  },
  {
    dataField: "",
    text: "Edit",
    formatter: EditButton,
    headerStyle: () => {
      return { ...commonStylesForColumn, width: "100px" };
    },
    style: { ...styleForRows },
  },
];
