import { useHistory } from "react-router-dom";
import { useState, useEffect } from "react";
import styles from "./Add.module.scss";
import { Alert } from "reactstrap";
import { toast } from "react-toastify";
import AddUnemployed from "../../services/addUnemployed";
import FormContainer from "../../components/Form/Form";
import Header from "../../components/Header/Header";

const Add = () => {
  const history = useHistory();
  useEffect(() => {
    if (localStorage.getItem("isLogin") === "false")
      return history.replace("/login");
  }, []);
  const [values, setValues] = useState({
    firstName: "",
    lastName: "",
    passportID: "",
    activity: "Worker",
    identificationCode: "",
  });

  const handleChange = (e) => {
    const { target } = e;
    if (!target)
      return setValues({
        ...values,
        activity: e.label,
      });
    setValues({
      ...values,
      [target.name]: target.value,
    });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (values.identificationCode.length < 10 || values.passportID.length < 9)
      return toast.error("Invalid information!");
    toast.success("Successfully added!");
    AddUnemployed.add(values);
    setValues({
      firstName: "",
      lastName: "",
      passportID: "",
      activity: "Worker",
      identificationCode: "",
    });
  };

  return (
    <>
      <Header />
      <div className={styles.wrapper}>
        <Alert color="success" className={styles.alert}>
          Please, fill in the form correctly!
        </Alert>
        <FormContainer
          handleSubmit={handleSubmit}
          handleChange={handleChange}
          values={values}
          btnTitle="Add unemployed"
        />
      </div>
    </>
  );
};

export default Add;
