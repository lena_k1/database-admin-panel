import { useLocation, useHistory } from "react-router-dom";
import { useState, useEffect } from "react";
import styles from "./Edit.module.scss";
import { Alert } from "reactstrap";
import { toast } from "react-toastify";
import FormContainer from "../../components/Form/Form";
import GetUnemployed from "../../services/getUnemployed";
import UpdateUnemployed from "../../services/updateUnemployed";
import DeleteUnemployed from "../../services/deleteUnemployed";
import Header from "../../components/Header/Header";

const Edit = () => {
  const loc = useLocation();
  const history = useHistory();
  const id = loc.pathname.replace("/edit/", "");

  const [values, setValues] = useState({
    firstName: "",
    lastName: "",
    passportID: "",
    activity: "",
    identificationCode: "",
  });

  useEffect(() => {
    if (localStorage.getItem("isLogin") === "false")
      return history.replace("/login");
    async function fetchData() {
      const data = await GetUnemployed.find(id);
      setValues(data.data.data);
    }
    fetchData();
  }, []);

  const handleChange = (e) => {
    const { target } = e;
    if (!target)
      return setValues({
        ...values,
        activity: e.value,
      });
    setValues({
      ...values,
      [target.name]: target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (values.identificationCode.length < 10 || values.passportID.length < 9)
      return toast.error("Invalid information!");
    toast.success("Successfully updated!");
    UpdateUnemployed.update(values);
  };

  const handleDeleteButton = () => {
    DeleteUnemployed.delete(id);
    toast.success("Successfully deleted!");
    setTimeout(() => {
      history.push("/");
    }, 2000);
  };

  return (
    <>
    <Header />
    <div className={styles.wrapper}>
      <Alert color="success" className={styles.alert}>
        Please fill in the form correctly!
      </Alert>
      <FormContainer
        handleSubmit={handleSubmit}
        handleChange={handleChange}
        values={values}
        btnTitle="Save changes"
        isDelete={true}
        handleDeleteButton={handleDeleteButton}
        edit={true}
      />
    </div>
    </>
  );
};

export default Edit;
