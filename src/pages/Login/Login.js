import { useHistory } from "react-router-dom";
import { useState, useEffect } from "react";
import styles from "./Login.module.scss";
import { Alert } from "reactstrap";
import { toast } from "react-toastify";
import LoginService from "../../services/login";
import FormContainer from "../../components/Form/Form";
import { loginInputs } from "../../config/constants";

const Login = () => {
  const history = useHistory();
  const [values, setValues] = useState({
    email: "",
    password: "",
  });
  useEffect(() => {
    if (localStorage.getItem("isLogin") === "true") history.replace("/");
  }, []);

  const handleChange = (e) => {
    const { target } = e;
    setValues({
      ...values,
      [target.name]: target.value,
    });
  };
  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      await LoginService.login(values);
      toast.success("Logged in!");
      localStorage.setItem("isLogin", true);
      setValues({
        email: "",
        password: "",
      });
      history.push("/");
    } catch {
      toast.error("Invalid information!");
    }
  };

  return (
    <div className={styles.wrapper}>
      <Alert color="success" className={styles.alert}>
        Please, log in to start!
      </Alert>
      <FormContainer
        handleSubmit={handleSubmit}
        handleChange={handleChange}
        values={values}
        btnTitle="Log In"
        arrayOfFields={loginInputs}
      />
    </div>
  );
};

export default Login;
