import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import styles from "./Home.module.scss";
import AllUnemployed from "../../services/getAllUnemployed";
import Header from "../../components/Header/Header";
import Table from "../../components/Table/Table";

const Home = () => {
  const history = useHistory();
  useEffect(() => {
    if (localStorage.getItem("isLogin") === "false") history.replace("/login");
  }, []);

  const [unemployed, setUnemployed] = useState([]);
  useEffect(() => {
    async function fetchData() {
      const data = await AllUnemployed.get();
      setUnemployed(data.data.data);
    }
    fetchData();
  }, []);

  return (
    <>
      <Header />
      <div className={styles.tableWrapper}>
        <Table data={unemployed} />
      </div>
    </>
  );
};

export default Home;
