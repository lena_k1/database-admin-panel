import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { ToastContainer } from "react-toastify";

import Home from "./pages/Home/Home";
import Add from "./pages/Add/Add";
import Edit from "./pages/Edit/Edit";
import Login from "./pages/Login/Login";
import "./styles/app.scss";

function App() {
  return (
    <Router>
      <div className="app">
        <Switch>
          <Route exact path="/add">
            <Add />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path={`/edit/:id`}>
            <Edit />
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
          <Redirect to="/" />
        </Switch>
        <ToastContainer autoClose={2000} draggable={false} />
      </div>
    </Router>
  );
}

export default App;
